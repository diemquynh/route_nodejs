let knex = require('knex')({
    client: 'mysql',
    connection: {
      host : /* '192.168.10.13' */'192.168.6.81',
      user : 'test',
      password : '123456',
      database : /* 'test_project' */'employee_management'
    }
    // ,
    // userParams: {
    //   userParam1: '451'
    // }
  });

  
module.exports = knex

// console.log(knex.userParams.userParam1)

// QUERY BUILDER
//   Lấy tất cả các bản ghi
//   let all = knex.select().from('users').orderBy('name','desc').limit(2)
//   console.log(all.toString())
//   console.log(all.toSQL().toNative())

// Lấy các bản ghi theo điều kiện
// knex('users').where('name', 'like', 'T%').pluck('id')

// groupBy
// knex('users').where('name', 'like', 'T%').groupBy('name').select('name',knex.raw("count(id)")).then(function(rows) { 
//     for(let i of rows){
//         console.log(i)
//     }
// });

// Hàm count(), countDistinct, min(), max(), sum()
// knex('users').min({min: 'id'}).first().then(result => {console.log(result.min)})

//   JOIN TABLE
// knex.select('userId','content').from('users').innerJoin('comments','users.id','comments.userId')
// knex.select().from('users').join('comments',{'users.id':'comments.userId'})
// knex('users').join('comments', 'users.id', '=', 'comments.userId').select('id', 'content').then(function(rows) {
//     for(let i of rows){
//         for(let j in i){
//             console.log(i[j])
//         }
//       }
// })

// INSERT => trả lại id bản ghi
// knex('users').insert({id: 4, name:'Sau', address:'Bac Ninh', age:20, gender:'Nam'}).then(result => {
//     console.log(result)
// }).catch(function(e){
//     console.log(e.code)
// })

// DELETE => trả lại số bản ghi được xóa
// knex('users').where('id', 6).del().then(result => { console.log(result) })

//  UPDATE => trả lại số bản ghi được update
// knex('users').where('id', 6).update('age',26).then(result => { console.log(result) })

// SCHEMA BUILDER
/* knex.schema.createTable('departments',function(table) {
  table.increments('departmentId')
  table.string('name',50)
  table.timestamps()
}).then() */

/* knex.schema.createTable('users',function(table) {
  table.increments('userId')
  table.string('name',50)
  table.float('salary')
  table.integer('departmentId').notNullable().unsigned()
  table.foreign('departmentId').references('departmentId').inTable('departments').onDelete('cascade')
  table.timestamps()
}).then() */

/* knex.schema.dropTableIfExists('users').then(function() {
  knex.schema.hasTable('users').then(exists => {
    if(exists) console.log('true')
    else console.log('false')
  })
}) */

/* knex.schema.table('users',function(table) {
  // table.dropColumn('name')
  // table.string('username',50).after('userId')
  table.renameColumn('username','name')
}).then().catch((err) => {console.log(err)}) */

// knex.schema.alterTable('users', table => table.integer('salary').alter()).then().catch(err => console.log(err))

/* knex.schema.table('employees',function(table){
  table.index('salary')
}).then().catch(err => console.log('An error has occured')) */