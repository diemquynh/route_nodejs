var http = require('http');
var url = require('url');
var fs = require('fs');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write('Hello World<br>');
  // Hiển thị đường dẫn url
  res.write(req.url + '<br>');
  // URL
  let q = url.parse(req.url, true);
  // Lấy path
  res.write(q.pathname + '<br>');
  // Lấy phần tham số dạng string, vd: ?name=Diem&age=20
  res.write(q.search + '<br>');
  // Lấy tham số trên url dạng object
  let txt = 'Name: ' + q.query.name + ", Age: " + q.query.age;
  res.write(txt);

  res.end();
}).listen(8080,'127.0.0.1', () => {});