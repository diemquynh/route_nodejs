const express = require('express');  
const app = express();  
app.listen(3000);
// route get
app.get('/user', function (req, res) {
    res.send('hello world')
})

// route có tham số
app.get('/user/:id(\\d+)', function (req, res) {
    res.send(req.params);
})

app.get('/user/:name', function (req, res) {
    res.send(req.params);
})

// Truyền nhiều fuction
app.get('/example/b', function (req, res, next) {
    console.log('the response will be sent by the next function ...')
    next()
  }, function (req, res) {
    res.send('Hello from B!')
})

function c1(req, res, next) {
    console.log('the response will be sent by the next function ...')
    next()
}
function c2(req, res) {
    res.send('Hello from B!')
}
app.get('/example/c', [c1,c2]);

// module
var birds = require('./module_route');

app.use('/module-birds/:id',birds);
//
app.use('/users',birds);