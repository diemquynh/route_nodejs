//viết router
const UserController = require('./../app/Controller/UserController')

const express = require('express')
const router = express.Router()

const user = new UserController()
router.use('/',  user.index)

module.exports = router